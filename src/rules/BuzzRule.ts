import { IRule } from "./IRule";

export class BuzzRule implements IRule {
  Operation(number: number): boolean {
    return number%5 == 0;
  }
  Result(): string {
    return "BUZZ";
  }
}