export interface IRule {
  Operation(number: number): boolean,
  Result(): string;
}