import { IRule } from "./IRule";

export class FizzBuzzRule implements IRule {
  Operation(number: number): boolean {
    return number%3 == 0 && number%5 == 0;
  }
  Result(): string {
    return "FIZZBUZZ";
  }
}