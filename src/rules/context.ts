import { BuzzRule } from "./BuzzRule";
import { FizzBuzzRule } from "./FizzBuzzRule";
import { FizzRule } from "./FizzRule";
import { IRule } from "./IRule";
import { Dictionary } from 'lodash';

export class Context {
  private rules: Array<IRule>;

  constructor() {
    this.rules = [];
    this.rules[0] = new FizzRule();
    this.rules[1] = new BuzzRule();
    this.rules[2] = new FizzBuzzRule();
  }

  public getResult(number: number): string {
    let result = number.toString();
    this.rules.forEach(rule => {
      if(rule.Operation(number)) {
        result = rule.Result();
      }
    });
    return result;
  }
  
}