import { IRule } from "./IRule";

export class FizzRule implements IRule {
  Operation(number: number): boolean {
    return number%3 == 0;
  }
  Result(): string {
    return "FIZZ";
  }
}