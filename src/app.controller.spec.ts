import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Context } from './rules/context';

describe('AppController', () => {
  let appController: AppController;
  let service: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService, Context],
    }).compile();

    appController = app.get<AppController>(AppController);
    service = app.get<AppService>(AppService);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });

    it('should return 1 if I pass the number 1', () => {
      const result = service.getFizzBuzz(1);
      expect("1").toBe(result);
    });

    it('should return 2 if I pass the number 2', () => {
      const result = service.getFizzBuzz(2);
      expect("2").toBe(result);
    });

    it('should return FIZZ if I pass the number 3', () => {
      const result = service.getFizzBuzz(3);
      expect("FIZZ").toBe(result);
    });

    it('should return 4 if I pass the number 4', () => {
      const result = service.getFizzBuzz(4);
      expect("4").toBe(result);
    });

    it('should return BUZZ if I pass the number 5', () => {
      const result = service.getFizzBuzz(5);
      expect("BUZZ").toBe(result);
    });

    it('should return FIZZ if I pass the number 6', () => {
      const result = service.getFizzBuzz(6);
      expect("FIZZ").toBe(result);
    });

    it('should return 7 if I pass the number 7', () => {
      const result = service.getFizzBuzz(7);
      expect("7").toBe(result);
    });

    it('should return FIZZBUZZ if I pass the number 15', () => {
      const result = service.getFizzBuzz(15);
      expect("FIZZBUZZ").toBe(result);
    });

  });

});
