import { Injectable } from '@nestjs/common';
import { Context } from './rules/context';

// Develop a function that 
// prints Fizz if the number is divisible by 3, 
// prints Buzz if the number is divisible by 5 
// and prints FizzBuzz if it is divisible by both, in any other case it should show the number
@Injectable()
export class AppService {

  constructor(private context: Context) {
  }

  getHello(): string {
    return 'Hello World!';
  }

  getFizzBuzz(number: number): string {
    return this.context.getResult(number);
  }

}